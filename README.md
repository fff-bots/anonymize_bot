# anonymize_bot
## Setup
Docker setup is reccomended. Docker images will be build automatically to this projects registry.

You need to set these variables:
 * anonymize_bot_username
 * anonymize_bot_token

One-command deploy:

`docker run -e anonymize_bot_username='@fff_anonym_bot' -e anonymize_bot_token='TOKEN' --restart always registry.gitlab.com/fff-bots/anonymize_bot`
